# Lab SCU02. The crate is Prototype
require stream
require scu
require essioc


epicsEnvSet("STREAM_PROTOCOL_PATH","${scu_DB}")

epicsEnvSet("SCU",     "SCU02")
epicsEnvSet("BRD_A",     "A")
epicsEnvSet("BRD_B",     "B")
epicsEnvSet("IP_ADDR_A", "172.30.6.173")
epicsEnvSet("IP_ADDR_B", "172.30.6.174")
epicsEnvSet("PIPELINE_ID", "201378")


epicsEnvSet("MC_TYPE1", "rs485mc")
epicsEnvSet("MC_TYPE2", "rs485mc")
epicsEnvSet("MC_TYPE3", "rs485mc")
epicsEnvSet("MC_TYPE4", "rs485mc")
epicsEnvSet("MC_TYPE5", "clmc")
epicsEnvSet("MC_TYPE6", "rs485mc")
epicsEnvSet("MC_TYPE7", "rs485mc")
epicsEnvSet("MC_TYPE8", "dymc")
epicsEnvSet("MC_TYPE9", "dymc")
epicsEnvSet("MC_TYPE10", "chmc")
epicsEnvSet("MC_TYPE11", "chmc")
epicsEnvSet("MC_TYPE12", "dymc")

epicsEnvSet("PORT_A", "FBIS-$(SCU)-$(BRD_A)")
epicsEnvSet("PORT_B", "FBIS-$(SCU)-$(BRD_B)")
epicsEnvSet("P", "FBIS-$(SCU):")

drvAsynIPPortConfigure("$(PORT_A)",     "$(IP_ADDR_A):8080", 0,0,1)
drvAsynIPPortConfigure("$(PORT_B)",     "$(IP_ADDR_B):8080", 0,0,1)

iocshLoad("$(scu_DIR)SCU.iocsh", "PORT=$(PORT_A), P=$(P), R=Ctrl-Ser-01:, BRD=$(BRD_A)")
iocshLoad("$(scu_DIR)SCU.iocsh", "PORT=$(PORT_B), P=$(P), R=Ctrl-Ser-02:, BRD=$(BRD_B)")

iocshLoad("$(scu_DIR)SCU_man.iocsh", "PORT=$(PORT_A), PORT_A=$(PORT_A), PORT_B=$(PORT_B), P=$(P)")

#asynSetTraceIOMask(FBIS-$(SCU)-$(BRD_A), 0, 4)
#asynSetTraceMask(FBIS-$(SCU)-$(BRD_A), 0, 0xf)

#- Check queue size with callbackQueueShow in a running IOC console. Default is 2000. Was giving "scanOnce: Ring buffer overflow" errors
callbackSetQueueSize(5000)
scanOnceSetQueueSize(5000)

iocInit
