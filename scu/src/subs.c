#include <stdio.h>
#include <inttypes.h>

#include <dbDefs.h>
#include <registryFunction.h>
#include <aSubRecord.h>
#include <epicsExport.h>

#include <string.h>

static long eeprom_mux(aSubRecord *prec)
{
    // Inputs
    uint8_t selector = *(uint8_t*)prec->a;
    char* manuf = (char*)prec->b;
    char* name = (char*)prec->c;
    char* rev = (char*)prec->d;
    char* sn = (char*)prec->e;
    uint32_t op_hrs = *(uint32_t*)prec->f;
    uint32_t op_hrs_fanA = *(uint32_t*)prec->g;
    uint32_t op_hrs_fanB = *(uint32_t*)prec->h;
    char* mac_addr = (char*)prec->i;

    // Variables
    char data_w[33];

    // Logic
    switch (selector) {
        case 1: // Manufacturer
            if (strlen(manuf)>32) manuf[32]='\0';
            strcpy(data_w, manuf);
            break;
        case 2: // Name
            if (strlen(name)>16) name[16]='\0';
            strcpy(data_w, name);
            break;
        case 3: // Revision
            if (strlen(rev)>16) rev[16]='\0';
            strcpy(data_w, rev);
            break;
        case 4: // Serial number
            if (strlen(sn)>16) sn[16]='\0';
            strcpy(data_w, sn);
            break;
        case 5: // Operating hours
            sprintf(data_w, "%d", op_hrs);
            break;
        case 6: // Operating hours fan A
            sprintf(data_w, "%d", op_hrs_fanA);
            break;
        case 7: // Operating hours fan B
            sprintf(data_w, "%d", op_hrs_fanB);
            break;
        case 8: // MAC address
            if (strlen(mac_addr)>17) mac_addr[17]='\0';
            strcpy(data_w, mac_addr);
            break;
    }
    memset(prec->vala, 0x20, 32);
    memcpy(prec->vala, data_w, strlen(data_w));
    return 0;
}

static long pad_zero_if_invalid(aSubRecord *prec)
{
    uint32_t *b = (uint32_t*)prec->b;

	uint32_t sevr = b[0];
	uint32_t n = prec->noa;

	if (sevr == 0 )
       memcpy(prec->vala, (uint32_t*)prec->a, n * sizeof(uint32_t));
	else
	   memset(prec->vala, 0, n * sizeof(uint32_t));

    return 0;
}


epicsRegisterFunction(eeprom_mux);
epicsRegisterFunction(pad_zero_if_invalid);
