#include <stdio.h>
#include <inttypes.h>

#include <dbDefs.h>
#include <registryFunction.h>
#include <aSubRecord.h>
#include <epicsExport.h>

#include <string.h>

#define DEBUG 0

typedef struct hb_record_t{
    uint32_t record_0;
    uint32_t record_1;
    uint32_t record_2;
    uint32_t record_3;
} hb_record_t;

char hb_dates[1024][40];
char hb_nanosec[1024][40];
uint8_t hb_status[1024];
char hb_module_type_name[1024][40];
char hb_type[1024][40];
char hb_subtype[1024][40];
uint32_t hb_details[1024];

void set_module_type_name(uint8_t mc_type_index, char* module_name){
    char name[40]="Unknown";
    switch (mc_type_index) {
        case  0: strcpy(name, "Serializer"); break;
        case  1: strcpy(name, "BUMC"      ); break;
        case  4: strcpy(name, "ISMC_1"    ); break;
        case  5: strcpy(name, "ISMC_2"    ); break;
        case  6: strcpy(name, "CLMC"      ); break;
        case  7: strcpy(name, "CHMC"      ); break;
        case  9: strcpy(name, "DYMC"      ); break;
        case 10: strcpy(name, "RS485MC"   ); break;
        case 11: strcpy(name, "LVDSMC"    ); break;
        case 12: strcpy(name, "RS485MC_2" ); break;
        default: break;
    }
    strcpy(module_name, name);
}

static int decode_hb_asub(aSubRecord *precord) {
    if (DEBUG>0) printf("Function decode_hb_asub:\n");
    // Inputs
    hb_record_t *hb_records = (hb_record_t*)precord->a;
    uint16_t first_rec = *(uint16_t*)precord->b;
    uint16_t nb_rec = *(uint16_t*)precord->c;
    // Variables
    uint16_t new_first_rec;
    // Logic
    int16_t ind; // needs to be negative for the for loop
    uint16_t car;
    time_t date;
    char ns[10];
    uint8_t ns_len;
    uint8_t module_type;
    uint8_t type, subtype;
    // Shift records
    for (ind=1024-nb_rec-1; ind>=0; ind--) {
        strcpy(hb_dates[ind+nb_rec], hb_dates[ind]);
        strcpy(hb_nanosec[ind+nb_rec], hb_nanosec[ind]);
        hb_status[ind+nb_rec] = hb_status[ind];
        strcpy(hb_module_type_name[ind+nb_rec], hb_module_type_name[ind]);
        strcpy(hb_type[ind+nb_rec], hb_type[ind]);
        strcpy(hb_subtype[ind+nb_rec], hb_subtype[ind]);
        hb_details[ind+nb_rec] = hb_details[ind];
    }
    // Add the new records
    for (ind=nb_rec-1; ind>=0; ind--) {
        if (DEBUG>1) printf("\tRecord %d: %08x %08x %08x %08x. ", ind, hb_records[nb_rec-ind-1].record_0, hb_records[nb_rec-ind-1].record_1, hb_records[nb_rec-ind-1].record_2, hb_records[nb_rec-ind-1].record_3);
        // timestamp
        date = (time_t)(hb_records[nb_rec-ind-1].record_0);
        strftime(hb_dates[ind], 20, "%Y-%m-%d %H:%M:%S", localtime(&date));
        if (DEBUG>1) printf("timestamp=%s, ", hb_dates[ind]);
        // nanosecond
        sprintf(ns, "%d", (int) (8.192*(hb_records[nb_rec-ind-1].record_1>>4)));
        memset(hb_nanosec[ind], ' ', 11);
        hb_nanosec[ind][11]='\0';
        ns_len = strlen(ns);
        for (car=0; car<ns_len; car++) // write digits one by one, jumping index every 3
            *((hb_nanosec[ind])+(10-car-(car/3))) = ns[ns_len-car-1];
        if (DEBUG>1) printf("nanosec=%s, ", hb_nanosec[ind]);
        // status
        hb_status[ind] = (hb_records[nb_rec-ind-1].record_1%4);
        if (DEBUG>1) printf("status=%d, ", hb_status[ind]);
        // Module identification
        module_type = (hb_records[nb_rec-ind-1].record_2>>16)%16;
        set_module_type_name(module_type, hb_module_type_name[ind]);
        if (DEBUG>1) printf("module=%s, ", hb_module_type_name[ind]);
        // HB record type/subtype
        type = (hb_records[nb_rec-ind-1].record_2>>8)%256;
        subtype = (hb_records[nb_rec-ind-1].record_2)%256;
        strcpy(hb_type[ind], "\0");
        strcpy(hb_subtype[ind], "\0");
        if (module_type==0) // Serializer
            switch (type) {
                case 1:
                    strcpy(hb_type[ind], "State");
                    switch (subtype) {
                        case 1: strcpy(hb_subtype[ind], "Startup"); break;
                        case 2: strcpy(hb_subtype[ind], "PBIT"); break;
                        case 3: strcpy(hb_subtype[ind], "Operational_Normal"); break;
                        case 4: strcpy(hb_subtype[ind], "Operational_Warning"); break;
                        case 5: strcpy(hb_subtype[ind], "Error"); break;
                        case 6: strcpy(hb_subtype[ind], "Fatal_Error"); break;
                        case 7: strcpy(hb_subtype[ind], "Shutdown"); break;
                        default:;
                    }
                    break;
                case 2:
                    strcpy(hb_type[ind], "BSO");
                    switch (subtype) {
                        case  1: strcpy(hb_subtype[ind], "SW BI"); break;
                        case  2: strcpy(hb_subtype[ind], "SW RBI"); break;
                        case  3: strcpy(hb_subtype[ind], "SW EBI"); break;
                        case  4: strcpy(hb_subtype[ind], "OPL BI"); break;
                        case  5: strcpy(hb_subtype[ind], "OPL BI"); break;
                        case  6: strcpy(hb_subtype[ind], "OPL BI"); break;
                        case  7: strcpy(hb_subtype[ind], "BI"); break;
                        case  8: strcpy(hb_subtype[ind], "RBI"); break;
                        case  9: strcpy(hb_subtype[ind], "EBI"); break;
                        case 10: strcpy(hb_subtype[ind], "GBP"); break;
                        default:;
                    }
                    break;
                case 3: strcpy(hb_type[ind], "UTC");
                    switch (subtype) {
                        case  1: strcpy(hb_subtype[ind], "Timestamp"); break;
                        case  2: strcpy(hb_subtype[ind], "Pulse per second"); break;
                        default:;
                    }
                    break;
                case 4: strcpy(hb_type[ind], "HB mask");
                    switch (subtype) {
                        case  1: strcpy(hb_subtype[ind], "HB masks"); break;
                        default:;
                    }
                    break;
                default:;
            }
        else {
            sprintf(hb_type[ind], "slot %d", type-4);
            switch (module_type) { // MC type
                case 1: // BUMC
                    switch (subtype) {
                        case  1: strcpy(hb_subtype[ind], "Enable state");break;
                        case  2: strcpy(hb_subtype[ind], "Driver state");break;
                        default: strcpy(hb_subtype[ind], "!! type not defined !!");
                    }
                    break;
                case 4: // ISMC_1
                    switch (subtype) {
                        case  1: strcpy(hb_subtype[ind], "Enable state");break;
                        case  2: strcpy(hb_subtype[ind], "Driver state");break;
                        case  3: strcpy(hb_subtype[ind], "Trigger from TS"); break;
                        case  4: strcpy(hb_subtype[ind], "Optical_Switch"); break;
                        case  5: strcpy(hb_subtype[ind], "Trigger_to_MagPS"); break;
                        case  6: strcpy(hb_subtype[ind], "FB_MagPS_Pwr"); break;
                        case  7: strcpy(hb_subtype[ind], "FSU_Cmd"); break;
                        case  8: strcpy(hb_subtype[ind], "FB_RF_PCB_Power"); break;
                        case  9: strcpy(hb_subtype[ind], "Force_Optical_Switch"); break;
                        case 10: strcpy(hb_subtype[ind], "Force_FSU_Cmd"); break;
                        case 11: strcpy(hb_subtype[ind], "Force_FB_MagPS_Pwr"); break;
                        case 12: strcpy(hb_subtype[ind], "Force_FB_RF_PCB_Power"); break;
                        case 13: strcpy(hb_subtype[ind], "FB_MagPS_Pwr_start_glitching"); break;
                        case 14: strcpy(hb_subtype[ind], "FB_MagPS_Pwr_stop_glitching"); break;
                        case 15: strcpy(hb_subtype[ind], "FB_RF_PCB_Power_start_glitching"); break;
                        case 16: strcpy(hb_subtype[ind], "FB_RF_PCB_Power_stop_glitching"); break;
                        case 31: strcpy(hb_subtype[ind], "HB mask set"); break;
                        default: strcpy(hb_subtype[ind], "!! type not defined !!");
                    }
                    break;
                case 5: // ISMC_2
                    switch (subtype) {
                        case  1: strcpy(hb_subtype[ind], "Enable state");break;
                        case  2: strcpy(hb_subtype[ind], "Driver state");break;
                        case  3: strcpy(hb_subtype[ind], "Switch_A"); break;
                        case  4: strcpy(hb_subtype[ind], "Switch_B"); break;
                        case  5: strcpy(hb_subtype[ind], "HVPS_Cmd"); break;
                        case  6: strcpy(hb_subtype[ind], "Force_HVPS_Cmd"); break;
                        case  7: strcpy(hb_subtype[ind], "Switch_A_start_glitching"); break;
                        case  8: strcpy(hb_subtype[ind], "Switch_A_stop_glitching"); break;
                        case  9: strcpy(hb_subtype[ind], "Switch_B_start_glitching"); break;
                        case 10: strcpy(hb_subtype[ind], "Switch_B_stop_glitching"); break;
                        case 31: strcpy(hb_subtype[ind], "HB mask set"); break;
                        default: strcpy(hb_subtype[ind], "!! type not defined !!");
                    }
                    break;
                case 6: // CLMC
                    switch (subtype) {
                        case  1: strcpy(hb_subtype[ind], "Enable state");break;
                        case  2: strcpy(hb_subtype[ind], "Driver state");break;
                        case  3: strcpy(hb_subtype[ind], "Discrete_BP"); break;
                        case  4: strcpy(hb_subtype[ind], "Discrete_Rdy"); break;
                        case  5: strcpy(hb_subtype[ind], "Discrete_Red_BP"); break;
                        case  6: strcpy(hb_subtype[ind], "Discrete_Red_Rdy"); break;
                        case  7: strcpy(hb_subtype[ind], "DL_BP"); break;
                        case  8: strcpy(hb_subtype[ind], "DL_Red_BP"); break;
                        case  9: strcpy(hb_subtype[ind], "DL_Rdy"); break;
                        case 10: strcpy(hb_subtype[ind], "DL_Red_Rdy"); break;
                        case 11: strcpy(hb_subtype[ind], "DL_PBD"); break;
                        case 12: strcpy(hb_subtype[ind], "DL_PBM"); break;
                        case 13: strcpy(hb_subtype[ind], "DL_Red_PBD"); break;
                        case 14: strcpy(hb_subtype[ind], "DL_Red_PBM"); break;
                        case 15: strcpy(hb_subtype[ind], "CC module Det"); break;
                        case 16: strcpy(hb_subtype[ind], "CC module ID"); break;
                        case 17: strcpy(hb_subtype[ind], "CC dec"); break;
                        case 18: strcpy(hb_subtype[ind], "CC FSM"); break;
                        case 19: strcpy(hb_subtype[ind], "CC sys"); break;
                        case 20: strcpy(hb_subtype[ind], "CC UART alive"); break;
                        case 21: strcpy(hb_subtype[ind], "CC top state"); break;
                        case 22: strcpy(hb_subtype[ind], "CC Anybus state"); break;
                        case 31: strcpy(hb_subtype[ind], "HB mask set"); break;
                        default: strcpy(hb_subtype[ind], "!! type not defined !!");
                    }
                    break;
                case 7: // CHMC
                    switch (subtype) {
                        case  1: strcpy(hb_subtype[ind], "Enable state");break;
                        case  2: strcpy(hb_subtype[ind], "Driver state");break;
                        case  3: strcpy(hb_subtype[ind], "Chop_cmd"); break;
                        case  4: strcpy(hb_subtype[ind], "Chop_A_readback"); break;
                        case  5: strcpy(hb_subtype[ind], "Chop_B_readback"); break;
                        case  6: strcpy(hb_subtype[ind], "Chop_AB_readback"); break;
                        case  7: strcpy(hb_subtype[ind], "FB_chop"); break;
                        case  8: strcpy(hb_subtype[ind], "Force_chop_cmd"); break;
                        case  9: strcpy(hb_subtype[ind], "Force_FB_chop"); break;
                        case 10: strcpy(hb_subtype[ind], "FB_chop"); break;
                        case 11: strcpy(hb_subtype[ind], "FB_chop"); break;
                        case 31: strcpy(hb_subtype[ind], "HB mask set"); break;
                        default: strcpy(hb_subtype[ind], "!! type not defined !!");
                    }
                    break;
                case 9: // DYMC
                    switch (subtype) {
                        case 31: strcpy(hb_subtype[ind], "HB mask set"); break;
                        default: strcpy(hb_subtype[ind], "!! type not defined !!");
                    }
                    break;
                case 10: // RS485MC
                    switch (subtype) {
                        case  1: strcpy(hb_subtype[ind], "Enable state");break;
                        case  2: strcpy(hb_subtype[ind], "Driver state");break;
                        case  3: strcpy(hb_subtype[ind], "CH1 Discrete_BP"); break;
                        case  4: strcpy(hb_subtype[ind], "CH1 Discrete_Rdy"); break;
                        case  5: strcpy(hb_subtype[ind], "CH1 Discrete_Red_BP"); break;
                        case  6: strcpy(hb_subtype[ind], "CH1 Discrete_Red_Rdy"); break;
                        case  7: strcpy(hb_subtype[ind], "CH2 Discrete_BP"); break;
                        case  8: strcpy(hb_subtype[ind], "CH2 Discrete_Rdy"); break;
                        case  9: strcpy(hb_subtype[ind], "CH2 Discrete_Red_BP"); break;
                        case 10: strcpy(hb_subtype[ind], "CH2 Discrete_Red_Rdy"); break;
                        case 31: strcpy(hb_subtype[ind], "HB mask set"); break;
                        default: strcpy(hb_subtype[ind], "!! type not defined !!");
                    }
                    break;
                case 11: // LVDSMC
                case 12: // RS485MC_2
                    switch (subtype) {
                        case  1: strcpy(hb_subtype[ind], "Enable state");break;
                        case  2: strcpy(hb_subtype[ind], "Driver state");break;
                        case  3: strcpy(hb_subtype[ind], "CH1 Discrete_BA_ISRC"); break;
                        case  4: strcpy(hb_subtype[ind], "CH1 Discrete_BP"); break;
                        case  5: strcpy(hb_subtype[ind], "CH1 Discrete_Rdy"); break;
                        case  6: strcpy(hb_subtype[ind], "CH1 DL_BA_ISRC"); break;
                        case  7: strcpy(hb_subtype[ind], "CH1 DL_BP"); break;
                        case  8: strcpy(hb_subtype[ind], "CH1 DL_Rdy"); break;
                        case  9: strcpy(hb_subtype[ind], "CH1 DL_PBD"); break;
                        case 10: strcpy(hb_subtype[ind], "CH1 DL_PBM"); break;
                        case 11: strcpy(hb_subtype[ind], "CH1 DL_HV_Absence"); break;
                        case 12: strcpy(hb_subtype[ind], "CH1 DL_HV_Ext"); break;
                        case 13: strcpy(hb_subtype[ind], "CH1 DL_BA_LEBT"); break;
                        case 14: strcpy(hb_subtype[ind], "CH1 DL_BA_MEBT"); break;
                        case 15: strcpy(hb_subtype[ind], "CH1 DL_HV_Ext_start_glitching"); break;
                        case 16: strcpy(hb_subtype[ind], "CH1 DL_HV_Ext_stop_glitching"); break;
                        case 17: strcpy(hb_subtype[ind], "CH2 Discrete_BA_ISRC"); break;
                        case 18: strcpy(hb_subtype[ind], "CH2 Discrete_BP"); break;
                        case 19: strcpy(hb_subtype[ind], "CH2 Discrete_Rdy"); break;
                        case 20: strcpy(hb_subtype[ind], "CH2 DL_BA_ISRC"); break;
                        case 21: strcpy(hb_subtype[ind], "CH2 DL_BP"); break;
                        case 22: strcpy(hb_subtype[ind], "CH2 DL_Rdy"); break;
                        case 23: strcpy(hb_subtype[ind], "CH2 DL_PBD"); break;
                        case 24: strcpy(hb_subtype[ind], "CH2 DL_PBM"); break;
                        case 25: strcpy(hb_subtype[ind], "CH2 DL_HV_Absence"); break;
                        case 26: strcpy(hb_subtype[ind], "CH2 DL_HV_Ext"); break;
                        case 27: strcpy(hb_subtype[ind], "CH2 DL_BA_LEBT"); break;
                        case 28: strcpy(hb_subtype[ind], "CH2 DL_BA_MEBT"); break;
                        case 29: strcpy(hb_subtype[ind], "CH2 DL_HV_Ext_start_glitching"); break;
                        case 30: strcpy(hb_subtype[ind], "CH2 DL_HV_Ext_stop_glitching"); break;
                        case 31: strcpy(hb_subtype[ind], "HB mask set"); break;
                        default: strcpy(hb_subtype[ind], "!! type not defined !!");
                    }
                    break;
                default:;
            }
        }
        if (DEBUG>1) printf("type=%s, ", hb_type[ind]);
        if (DEBUG>1) printf("subtype=%s, ", hb_subtype[ind]);
        // Details
        hb_details[ind] = hb_records[nb_rec-ind-1].record_3;
        if (DEBUG>1) printf("details=%08x\n", hb_details[ind]);
    }
    // shift the IOC first index to read for next time
    new_first_rec = ((first_rec+nb_rec)%1024) & 0xffff;
    if (DEBUG>0) printf("\tfirst_rec=%d, nb_rec=%d, new_first_rec=%d\n", first_rec, nb_rec, new_first_rec);
    // Outputs
    memcpy(precord->vala,   hb_dates, sizeof(hb_dates));
    memcpy(precord->valb, hb_nanosec, sizeof(hb_nanosec));
    memcpy(precord->valc,  hb_status, sizeof(hb_status));
    memcpy(precord->vald,  hb_module_type_name, sizeof(hb_module_type_name));
    memcpy(precord->vale,  hb_type, sizeof(hb_type));
    memcpy(precord->valf,  hb_subtype, sizeof(hb_subtype));
    memcpy(precord->valg,  hb_details, sizeof(hb_details));
    *((uint16_t *)(precord->valh)) = new_first_rec;
    return 0;
}

epicsRegisterFunction(decode_hb_asub);
