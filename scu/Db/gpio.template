## GPIO interface
# MODULE : Module to interface: 0(Serializer), 1-12 (MC 1 to 12), 13-15(SFP 1 to 3)
# GPIO_SIG_IDX: GPIO signal index within the specific slot. Ex for MC: 0(Reset), 1(Power Enable), 2(Presence)
# LABEL: Unique Label associated to the GPIO signal

## GPIO reading
record(waveform, "$(P)$(R)#gpio_rd_$(LABEL)")
{
	field(DESC, "GPIO $(LABEL) reading ")
	field(DTYP, "stream")
	field(INP,  "@scu.proto read_gpio($(MODULE), $(GPIO_SIG_IDX), $(P)$(R)#gpio_$(LABEL)) $(PORT)")
	field(FTVL, "CHAR")
	field(NELM, "1")
}

record(bi, "$(P)$(R)#gpio_$(LABEL)")
{
	field(DESC, "GPIO $(LABEL) value read")
}

record(calcout, "$(P)$(R)#gpio_c_$(LABEL)") {
	field(CALC, "B = 0 ? A : 0")
	field(INPA, "$(P)$(R)#gpio_$(LABEL)")
	field(INPB, "$(P)$(R)#gpio_rd_$(LABEL).SEVR")
	field(SCAN, "1 second")
	field(OUT,  "$(P)$(R)gpio_$(LABEL) PP")
	field(FLNK, "$(P)$(R)#gpio_rd_$(LABEL)")
}

record(bi, "$(P)$(R)gpio_$(LABEL)")
{
	field(DESC, "GPIO $(LABEL) value read")
}

## GPIO writing

record(bo, "$(P)$(R)gpio_wr_$(LABEL)_v")
{
	field(DESC, "GPIO $(LABEL) value to write")
}

record(waveform, "$(P)$(R)#gpio_wr_$(LABEL)")
{
	field(DESC, "GPIO $(LABEL) writing ")
	field(DTYP, "stream")
	field(INP,  "@scu.proto write_gpio($(MODULE), $(GPIO_SIG_IDX), $(P)$(R)gpio_wr_$(LABEL)_v, $(P)$(R)#gpio_wr_$(LABEL)_r) $(PORT)")
	field(FTVL, "CHAR")
	field(NELM, "4")
	field(PINI, "0")
	field(SCAN, "Passive")
}

record(longin, "$(P)$(R)#gpio_wr_$(LABEL)_r")
{
	field(DESC, "GPIO writing return")
}
