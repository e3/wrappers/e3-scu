# SCU RS485MC2 substitution file
#
# MACRO     = Description                                           (eg: example)
#
# PORT      = Connection port                                       172.30.4.255:8080
# P         = System-Sub system                                     FBIS-SCU-Lab2:
# R         = Rack ID                                               Ctrl-MC-007:
# ADDR      = Registers address offset                              0x0A00
# BRD       = Channel of the redundant system                       A/B


# MC generic PVs
file "SCU_mc_reg.template" { pattern
{   PORT,     P,     R,     ADDR,     BRD,  PINI,        SCAN}
{$(PORT),  $(P),  $(R),  $(ADDR),  $(BRD),     1,  "2 second"}
}

# RS485MC2 Ports PVs
file "rs485mc2_port.template" { pattern
{   PORT,    P,    R,    BRD, BASE_ADDR, STAT_IDX, STAT_IDX2, STAT_IDX3, STAT_IDX4, STAT_IDX5, CH}
{$(PORT), $(P), $(R), $(BRD),   $(ADDR),        0,         3,         6,         9,        12,  1},
{$(PORT), $(P), $(R), $(BRD),   $(ADDR),        1,         4,         7,        10,        13,  2}
}

# RS485MC2 status bits reading
file "read_stat_bit.template" { pattern
{    P,    R,    BRD,             NAME,                    DESC, STAT_IDX, BIT_IDX,  ZNAM,    ONAM,        ZSV,        OSV}
{ $(P), $(R), $(BRD), "RS2_P1_BA"     , "RS2 Port 1 BA"        ,        0,       0, "NOK", "OK"   , "NO_ALARM", "NO_ALARM"},
{ $(P), $(R), $(BRD), "RS2_P1_BP"     , "RS2 Port 1 BP"        ,        0,       1, "NOK", "OK"   , "NO_ALARM", "NO_ALARM"},
{ $(P), $(R), $(BRD), "RS2_P1_Rdy"    , "RS2 Port 1 Rdy"       ,        0,       2, "NOK", "OK"   , "NO_ALARM", "NO_ALARM"},
{ $(P), $(R), $(BRD), "RS2_P1_DL_err" , "RS2 Port 1 DL dec err",        3,      31, "OK" , "Error", "NO_ALARM", "NO_ALARM"},
{ $(P), $(R), $(BRD), "RS2_P2_BA"     , "RS2 Port 2 BA"        ,        1,       0, "NOK", "OK"   , "NO_ALARM", "NO_ALARM"},
{ $(P), $(R), $(BRD), "RS2_P2_BP"     , "RS2 Port 2 BP"        ,        1,       1, "NOK", "OK"   , "NO_ALARM", "NO_ALARM"},
{ $(P), $(R), $(BRD), "RS2_P2_Rdy"    , "RS2 Port 2 Rdy"       ,        1,       2, "NOK", "OK"   , "NO_ALARM", "NO_ALARM"},
{ $(P), $(R), $(BRD), "RS2_P2_DL_err" , "RS2 Port 2 DL dec err",        4,      31, "OK" , "Error", "NO_ALARM", "NO_ALARM"}
}

# RS485MC2 datalink signals reading (2 bits decoding)
file "read_dl_signal.template" { pattern
{    P,    R,    BRD, CH, MC_TYPE,  SIG_NAME, STAT_IDX, BIT_IDX}
{ $(P), $(R), $(BRD),  1,   "RS2", "BA"     ,        0,       3},
{ $(P), $(R), $(BRD),  1,   "RS2", "BP"     ,        0,       5},
{ $(P), $(R), $(BRD),  1,   "RS2", "Rdy"    ,        0,       8},
{ $(P), $(R), $(BRD),  1,   "RS2", "HVAbs"  ,        0,      10},
{ $(P), $(R), $(BRD),  1,   "RS2", "HVExt"  ,        0,      12},
{ $(P), $(R), $(BRD),  1,   "RS2", "BA_LEBT",        3,      24},
{ $(P), $(R), $(BRD),  1,   "RS2", "BA_MEBT",        3,      26},
{ $(P), $(R), $(BRD),  2,   "RS2", "BA"     ,        1,       3},
{ $(P), $(R), $(BRD),  2,   "RS2", "BP"     ,        1,       5},
{ $(P), $(R), $(BRD),  2,   "RS2", "Rdy"    ,        1,       8},
{ $(P), $(R), $(BRD),  2,   "RS2", "HVAbs"  ,        1,      10},
{ $(P), $(R), $(BRD),  2,   "RS2", "HVExt"  ,        1,      12},
{ $(P), $(R), $(BRD),  2,   "RS2", "BA_LEBT",        4,      24},
{ $(P), $(R), $(BRD),  2,   "RS2", "BA_MEBT",        4,      26}
}

# RS485MC2 override
# Ex: $(DESC)        = functional description of the bit                 = "Port 1 BA"
#     $(NAME)        = name given within the dedicated PVs (Max 16 char) = "RS2_P1_BA"
#     $(BASE_ADDR)   = Base address of the MC memory block               = 0x08
# Register identification:
#     $(CTRL_IDX)    = Register index among the RW regs (0 to 3)         = 0
#     $(CTRL_ADDR)   = Register address to add to $(BASE_ADDR)<<8        = 0x08
# Bit identification:
#     $(BIT_IDX)     = Index of the bit to change in the register        = 0
#     $(BYTE_MASK_x) = x=3(MSB) to 0(LSB). Has to fit $(BIT_IDX)         = 0x00, 0x00, 0x00, 0x01
file "override_mc_bit.template" { pattern
{   PORT,    P,    R,    BRD,                    DESC,               NAME, BASE_ADDR, CTRL_IDX, CTRL_ADDR, BIT_IDX, BYTE_MASK_3, BYTE_MASK_2, BYTE_MASK_1, BYTE_MASK_0}
{$(PORT), $(P), $(R), $(BRD), "RS2 Port 1 BA"        , "RS2_P1_BA"       ,   $(ADDR),        0,      0x08,       0,        0x00,        0x00,        0x00,        0x01},
{$(PORT), $(P), $(R), $(BRD), "RS2 Port 1 BP"        , "RS2_P1_BP"       ,   $(ADDR),        0,      0x08,       1,        0x00,        0x00,        0x00,        0x02},
{$(PORT), $(P), $(R), $(BRD), "RS2 Port 1 Rdy"       , "RS2_P1_Rdy"      ,   $(ADDR),        0,      0x08,       2,        0x00,        0x00,        0x00,        0x04},
{$(PORT), $(P), $(R), $(BRD), "RS2 Port 2 BA"        , "RS2_P2_BA"       ,   $(ADDR),        0,      0x08,       8,        0x00,        0x00,        0x01,        0x00},
{$(PORT), $(P), $(R), $(BRD), "RS2 Port 2 BP"        , "RS2_P2_BP"       ,   $(ADDR),        0,      0x08,       9,        0x00,        0x00,        0x02,        0x00},
{$(PORT), $(P), $(R), $(BRD), "RS2 Port 2 Rdy"       , "RS2_P2_Rdy"      ,   $(ADDR),        0,      0x08,      10,        0x00,        0x00,        0x04,        0x00},
{$(PORT), $(P), $(R), $(BRD), "RS2 Port 1 DL BA"     , "RS2_P1_DL_BA"    ,   $(ADDR),        1,      0x0C,       0,        0x00,        0x00,        0x00,        0x01},
{$(PORT), $(P), $(R), $(BRD), "RS2 Port 1 DL BP"     , "RS2_P1_DL_BP"    ,   $(ADDR),        1,      0x0C,       1,        0x00,        0x00,        0x00,        0x02},
{$(PORT), $(P), $(R), $(BRD), "RS2 Port 1 DL Rdy"    , "RS2_P1_DL_Rdy"   ,   $(ADDR),        1,      0x0C,       2,        0x00,        0x00,        0x00,        0x04},
{$(PORT), $(P), $(R), $(BRD), "RS2 Port 1 DL HVAbs"  , "RS2_P1_DL_HVAbs" ,   $(ADDR),        1,      0x0C,       3,        0x00,        0x00,        0x00,        0x08},
{$(PORT), $(P), $(R), $(BRD), "RS2 Port 1 DL HVExt"  , "RS2_P1_DL_HVExt" ,   $(ADDR),        1,      0x0C,       4,        0x00,        0x00,        0x00,        0x10},
{$(PORT), $(P), $(R), $(BRD), "RS2 Port 1 DL PBD"    , "RS2_P1_DL_PBD"   ,   $(ADDR),        1,      0x0C,       5,        0x00,        0x00,        0x00,        0x20},
{$(PORT), $(P), $(R), $(BRD), "RS2 Port 1 DL PBM"    , "RS2_P1_DL_PBM"   ,   $(ADDR),        1,      0x0C,       6,        0x00,        0x00,        0x00,        0x40},
{$(PORT), $(P), $(R), $(BRD), "RS2 Port 1 DL BA LEBT", "RS2_P1_DL_BALEBT",   $(ADDR),        1,      0x0C,       7,        0x00,        0x00,        0x00,        0x80},
{$(PORT), $(P), $(R), $(BRD), "RS2 Port 1 DL BA MEBT", "RS2_P1_DL_BAMEBT",   $(ADDR),        1,      0x0C,       8,        0x00,        0x00,        0x01,        0x00},
{$(PORT), $(P), $(R), $(BRD), "RS2 Port 2 DL BA"     , "RS2_P2_DL_BA"    ,   $(ADDR),        1,      0x0C,      10,        0x00,        0x00,        0x04,        0x00},
{$(PORT), $(P), $(R), $(BRD), "RS2 Port 2 DL BP"     , "RS2_P2_DL_BP"    ,   $(ADDR),        1,      0x0C,      11,        0x00,        0x00,        0x08,        0x00},
{$(PORT), $(P), $(R), $(BRD), "RS2 Port 2 DL Rdy"    , "RS2_P2_DL_Rdy"   ,   $(ADDR),        1,      0x0C,      12,        0x00,        0x00,        0x10,        0x00},
{$(PORT), $(P), $(R), $(BRD), "RS2 Port 2 DL HVAbs"  , "RS2_P2_DL_HVAbs" ,   $(ADDR),        1,      0x0C,      13,        0x00,        0x00,        0x20,        0x00},
{$(PORT), $(P), $(R), $(BRD), "RS2 Port 2 DL HVExt"  , "RS2_P2_DL_HVExt" ,   $(ADDR),        1,      0x0C,      14,        0x00,        0x00,        0x40,        0x00},
{$(PORT), $(P), $(R), $(BRD), "RS2 Port 2 DL PBD"    , "RS2_P2_DL_PBD"   ,   $(ADDR),        1,      0x0C,      15,        0x00,        0x00,        0x80,        0x00},
{$(PORT), $(P), $(R), $(BRD), "RS2 Port 2 DL PBM"    , "RS2_P2_DL_PBM"   ,   $(ADDR),        1,      0x0C,      16,        0x00,        0x01,        0x00,        0x00},
{$(PORT), $(P), $(R), $(BRD), "RS2 Port 2 DL BA LEBT", "RS2_P2_DL_BALEBT",   $(ADDR),        1,      0x0C,      17,        0x00,        0x02,        0x00,        0x00},
{$(PORT), $(P), $(R), $(BRD), "RS2 Port 2 DL BA MEBT", "RS2_P2_DL_BAMEBT",   $(ADDR),        1,      0x0C,      18,        0x00,        0x04,        0x00,        0x00}
}
