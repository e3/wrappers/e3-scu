# SCU RS485MC substitution file
#
# MACRO     = Description                                           (eg: example)
#
# PORT      = Connection port                                       172.30.4.255:8080
# P         = System-Sub system                                     FBIS-SCU-Lab2:
# R         = Rack ID                                               Ctrl-MC-007:
# ADDR      = Registers address offset                              0x0A00
# BRD       = Channel of the redundant system                       A/B


# MC generic PVs
file "SCU_mc_reg.template" { pattern
    {   PORT,     P,     R,     ADDR,     BRD,  PINI,        SCAN}
    {$(PORT),  $(P),  $(R),  $(ADDR),  $(BRD),     1,  "2 second"}
}

# RS485 Ports PVs
file "rs485mc_port.template" { pattern
{   PORT,    P,    R,    BRD, BASE_ADDR, STAT_IDX, STAT_IDX2, STAT_IDX3, CH}
{$(PORT), $(P), $(R), $(BRD),   $(ADDR),        1,         3,         7,  1},
{$(PORT), $(P), $(R), $(BRD),   $(ADDR),        2,         4,         8,  2}
}

# RS485 specific PVs
file "rs485mc_specific.template" { pattern
    {   PORT,     P,     R,     ADDR,     BRD}
    {$(PORT),  $(P),  $(R),  $(ADDR),  $(BRD)}
}

# RS485 status bits reading
file "read_stat_bit.template" { pattern
    {    P,    R,    BRD,            NAME,                   DESC, STAT_IDX, BIT_IDX,  ZNAM,    ONAM,        ZSV,        OSV}
    { $(P), $(R), $(BRD), "RS_P1_S0"     , "RS Port 1 Signal 0"  ,        0,       0, "NOK", "OK"   , "NO_ALARM", "NO_ALARM"},
    { $(P), $(R), $(BRD), "RS_P1_S1"     , "RS Port 1 Signal 1"  ,        0,       1, "NOK", "OK"   , "NO_ALARM", "NO_ALARM"},
    { $(P), $(R), $(BRD), "RS_P1_S2"     , "RS Port 1 Signal 2"  ,        0,       2, "NOK", "OK"   , "NO_ALARM", "NO_ALARM"},
    { $(P), $(R), $(BRD), "RS_P1_S3"     , "RS Port 1 Signal 3"  ,        0,       3, "NOK", "OK"   , "NO_ALARM", "NO_ALARM"},
    { $(P), $(R), $(BRD), "RS_P2_S0"     , "RS Port 2 Signal 0"  ,        0,       8, "NOK", "OK"   , "NO_ALARM", "NO_ALARM"},
    { $(P), $(R), $(BRD), "RS_P2_S1"     , "RS Port 2 Signal 1"  ,        0,       9, "NOK", "OK"   , "NO_ALARM", "NO_ALARM"},
    { $(P), $(R), $(BRD), "RS_P2_S2"     , "RS Port 2 Signal 2"  ,        0,      10, "NOK", "OK"   , "NO_ALARM", "NO_ALARM"},
    { $(P), $(R), $(BRD), "RS_P2_S3"     , "RS Port 2 Signal 3"  ,        0,      11, "NOK", "OK"   , "NO_ALARM", "NO_ALARM"},
    { $(P), $(R), $(BRD), "RS_alert_pow" , "RS Alert power"      ,        5,       0, "OK" , "Alarm", "NO_ALARM", "NO_ALARM"},
    { $(P), $(R), $(BRD), "RS_alert_IO"  , "RS Alert IO"         ,        5,       1, "OK" , "Alarm", "NO_ALARM", "NO_ALARM"},
    { $(P), $(R), $(BRD), "RS_alert_temp", "RS Alert temperature",        5,       2, "OK" , "Alarm", "NO_ALARM", "NO_ALARM"}
}

# RS485 datalink signals reading (2 bits decoding)
file "read_dl_signal.template" { pattern
{    P,    R,    BRD, CH, MC_TYPE,  SIG_NAME, STAT_IDX, BIT_IDX}
{ $(P), $(R), $(BRD),  1,    "RS", "S0"     ,        1,       0},
{ $(P), $(R), $(BRD),  1,    "RS", "S1"     ,        1,       2},
{ $(P), $(R), $(BRD),  1,    "RS", "S2"     ,        1,       4},
{ $(P), $(R), $(BRD),  1,    "RS", "S3"     ,        1,       6},
{ $(P), $(R), $(BRD),  2,    "RS", "S0"     ,        2,       0},
{ $(P), $(R), $(BRD),  2,    "RS", "S1"     ,        2,       2},
{ $(P), $(R), $(BRD),  2,    "RS", "S2"     ,        2,       4},
{ $(P), $(R), $(BRD),  2,    "RS", "S3"     ,        2,       6}
}

# RS485 override
# Ex: $(DESC)        = functional description of the bit          = "Port 1 signal 0"
#     $(NAME)        = name given within the dedicated PVs        = "RS_P1_S0"
#     $(BASE_ADDR)   = Base address of the MC memory block        = 0x07
# Register identification:
#     $(CTRL_IDX)    = Register index among the RW regs (0 to 3)  = 0
#     $(CTRL_ADDR)   = Register address to add to $(BASE_ADDR)<<8 = 0x08
# Bit identification:
#     $(BIT_IDX)     = Index of the bit to change in the register = 0
#     $(BYTE_MASK_x) = x=3(MSB) to 0(LSB). Has to fit $(BIT_IDX)  = 0x00, 0x00, 0x00, 0x01
file "override_mc_bit.template" { pattern
    {   PORT,    P,    R,    BRD,                 DESC,           NAME, BASE_ADDR, CTRL_IDX, CTRL_ADDR, BIT_IDX, BYTE_MASK_3, BYTE_MASK_2, BYTE_MASK_1, BYTE_MASK_0}
    {$(PORT), $(P), $(R), $(BRD), "RS Port 1 Signal 0", "RS_P1_S0"    ,   $(ADDR),        0,      0x08,       0,        0x00,        0x00,        0x00,        0x01},
    {$(PORT), $(P), $(R), $(BRD), "RS Port 1 Signal 1", "RS_P1_S1"    ,   $(ADDR),        0,      0x08,       1,        0x00,        0x00,        0x00,        0x02},
    {$(PORT), $(P), $(R), $(BRD), "RS Port 1 Signal 2", "RS_P1_S2"    ,   $(ADDR),        0,      0x08,       2,        0x00,        0x00,        0x00,        0x04},
    {$(PORT), $(P), $(R), $(BRD), "RS Port 1 Signal 3", "RS_P1_S3"    ,   $(ADDR),        0,      0x08,       3,        0x00,        0x00,        0x00,        0x08},
    {$(PORT), $(P), $(R), $(BRD), "RS Port 2 Signal 0", "RS_P2_S0"    ,   $(ADDR),        0,      0x08,       4,        0x00,        0x00,        0x00,        0x10},
    {$(PORT), $(P), $(R), $(BRD), "RS Port 2 Signal 1", "RS_P2_S1"    ,   $(ADDR),        0,      0x08,       5,        0x00,        0x00,        0x00,        0x20},
    {$(PORT), $(P), $(R), $(BRD), "RS Port 2 Signal 2", "RS_P2_S2"    ,   $(ADDR),        0,      0x08,       6,        0x00,        0x00,        0x00,        0x40},
    {$(PORT), $(P), $(R), $(BRD), "RS Port 2 Signal 3", "RS_P2_S3"    ,   $(ADDR),        0,      0x08,       7,        0x00,        0x00,        0x00,        0x80},
    {$(PORT), $(P), $(R), $(BRD), "RS Datalink reset" , "RS_DL_rst"   ,   $(ADDR),        0,      0x08,      31,        0x80,        0x00,        0x00,        0x00},
    {$(PORT), $(P), $(R), $(BRD), "RS P1 DL Signal 0" , "RS_P1_DL_S0" ,   $(ADDR),        1,      0x0C,       0,        0x00,        0x00,        0x00,        0x01},
    {$(PORT), $(P), $(R), $(BRD), "RS P1 DL Signal 1" , "RS_P1_DL_S1" ,   $(ADDR),        1,      0x0C,       1,        0x00,        0x00,        0x00,        0x02},
    {$(PORT), $(P), $(R), $(BRD), "RS P1 DL Signal 2" , "RS_P1_DL_S2" ,   $(ADDR),        1,      0x0C,       2,        0x00,        0x00,        0x00,        0x04},
    {$(PORT), $(P), $(R), $(BRD), "RS P1 DL Signal 3" , "RS_P1_DL_S3" ,   $(ADDR),        1,      0x0C,       3,        0x00,        0x00,        0x00,        0x08},
    {$(PORT), $(P), $(R), $(BRD), "RS P1 DL PBD"      , "RS_P1_DL_PBD",   $(ADDR),        1,      0x0C,       4,        0x00,        0x00,        0x00,        0x10},
    {$(PORT), $(P), $(R), $(BRD), "RS P1 DL PBM"      , "RS_P1_DL_PBM",   $(ADDR),        1,      0x0C,       5,        0x00,        0x00,        0x00,        0x20},
    {$(PORT), $(P), $(R), $(BRD), "RS P2 DL Signal 0" , "RS_P2_DL_S0" ,   $(ADDR),        1,      0x0C,       6,        0x00,        0x00,        0x00,        0x40},
    {$(PORT), $(P), $(R), $(BRD), "RS P2 DL Signal 1" , "RS_P2_DL_S1" ,   $(ADDR),        1,      0x0C,       7,        0x00,        0x00,        0x00,        0x80},
    {$(PORT), $(P), $(R), $(BRD), "RS P2 DL Signal 2" , "RS_P2_DL_S2" ,   $(ADDR),        1,      0x0C,       8,        0x00,        0x00,        0x01,        0x00},
    {$(PORT), $(P), $(R), $(BRD), "RS P2 DL Signal 3" , "RS_P2_DL_S3" ,   $(ADDR),        1,      0x0C,       9,        0x00,        0x00,        0x02,        0x00},
    {$(PORT), $(P), $(R), $(BRD), "RS P2 DL PBD"      , "RS_P2_DL_PBD",   $(ADDR),        1,      0x0C,      10,        0x00,        0x00,        0x04,        0x00},
    {$(PORT), $(P), $(R), $(BRD), "RS P2 DL PBM"      , "RS_P2_DL_PBM",   $(ADDR),        1,      0x0C,      11,        0x00,        0x00,        0x08,        0x00}
}
