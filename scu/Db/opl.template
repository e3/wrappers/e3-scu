# GBP ###################################################################

record(calcout, "$(P)$(R)#opl_gbp_$(BRD)")
{
    field(DESC, "SCU OPL GBP calc")
    field(INPA, "$(P)$(R)#r_conf_0x0044_$(BRD).VAL CPP")
    field(CALC, "A%2")
    field(OUT,  "$(P)$(R)opl_gbp_$(BRD)")
}
record(bi, "$(P)$(R)opl_gbp_$(BRD)")
{
    field(DESC, "SCU OPL GBP")
    field(INP,  "$(P)$(R)#opl_gbp_$(BRD) CPP")
    field(ZNAM, "Not OK")
    field(ONAM, "OK")
    field(ZSV,  "NO_ALARM")
    field(OSV,  "NO_ALARM")
}

# Clear ###################################################################

record(bo, "$(P)$(R)opl_clear_EC_$(BRD)")
{
    field(DESC, "Reset OPL error counters")
    field(ZNAM, "No reset")
    field(ONAM, "Reset")
    field(HIGH, "0.1")
    field(FLNK, "$(P)$(R)#opl_clear_EC_fo_$(BRD)")
}
record(fanout, "$(P)$(R)#opl_clear_EC_fo_$(BRD)")
{
    field(DESC, "Reset OPL error fanout")
    field(SELM, "All")
    field(LNKA, "$(P)$(R)#opl_clear_EC_v_$(BRD)")
    field(LNKB, "$(P)$(R)#opl_clear_EC_M_$(BRD)")
    field(LNKC, "$(P)$(R)#opl_clear_EC_$(BRD)")
}
record(calcout, "$(P)$(R)#opl_clear_EC_v_$(BRD)")
{
    field(DESC, "Reset OPL error counters val")
    field(INPA, "$(P)$(R)opl_clear_EC_$(BRD).VAL")
    field(CALC, "A")
}
record(longout, "$(P)$(R)#opl_clear_EC_M_$(BRD)")
{
    field(DESC, "Reset OPL error counters mask")
    field(VAL,  "0x01")
}
record(longout, "$(P)$(R)#opl_clear_EC_$(BRD)")
{
    field(DESC, "Reset OPL error counters")
    field(DTYP, "stream")
    field(OUT,  "@scu.proto write_reg(0x00, 0x78, $(P)$(R)#opl_clear_EC_v_$(BRD), $(P)$(R)#opl_clear_EC_M_$(BRD)) $(PORT)")
}

# OPL Upstream ##########################################################

# OPL states
record(mbbi, "$(P)$(R)opl_US_Tx_local_ste_$(BRD)")
{
    field (DESC, "OPL Upstream tx local state")
    field (DTYP, "Raw Soft Channel")
    field (INP,  "$(P)$(R)#r_conf_0x0044_$(BRD) CPP")
    field (NOBT, "15")
    field (MASK, "0x00000007")
    field (ZRST, "Error")
    field (ONST, "OK")
    field (TWST, "BI ")
    field (FRST, "RBI")
    field (SVST, "EBI")
    field (ZRSV, "MAJOR")
    field (THSV, "MAJOR")
    field (FVSV, "MAJOR")
    field (SXSV, "MAJOR")
    field (EISV, "MAJOR")
}
record(mbbi, "$(P)$(R)opl_US_Tx_NB_ste_$(BRD)")
{
    field (DESC, "OPL Upstream tx neighbour state")
    field (DTYP, "Raw Soft Channel")
    field (INP,  "$(P)$(R)#r_conf_0x0044_$(BRD) CPP")
    field (NOBT, "18")
    field (MASK, "0x00000007")
    field (ZRST, "Error")
    field (ONST, "OK")
    field (TWST, "BI ")
    field (FRST, "RBI")
    field (SVST, "EBI")
    field (ZRSV, "MAJOR")
    field (THSV, "MAJOR")
    field (FVSV, "MAJOR")
    field (SXSV, "MAJOR")
    field (EISV, "MAJOR")
}
record(mbbi, "$(P)$(R)opl_US_Rx_NB_ste_$(BRD)")
{
    field (DESC, "OPL Upstream rx neighbour state")
    field (DTYP, "Raw Soft Channel")
    field (INP,  "$(P)$(R)#r_conf_0x0044_$(BRD) CPP")
    field (NOBT, "9")
    field (MASK, "0x00000007")
    field (ZRST, "Error")
    field (ONST, "OK")
    field (TWST, "BI ")
    field (FRST, "RBI")
    field (SVST, "EBI")
    field (ZRSV, "MAJOR")
    field (THSV, "MAJOR")
    field (FVSV, "MAJOR")
    field (SXSV, "MAJOR")
    field (EISV, "MAJOR")
}
record(mbbi, "$(P)$(R)opl_US_Rx_NBAN_ste_$(BRD)")
{
    field (DESC, "OPL Upstream rx neighbour next state")
    field (DTYP, "Raw Soft Channel")
    field (INP,  "$(P)$(R)#r_conf_0x0044_$(BRD) CPP")
    field (NOBT, "12")
    field (MASK, "0x00000007")
    field (ZRST, "Error")
    field (ONST, "OK")
    field (TWST, "BI ")
    field (FRST, "RBI")
    field (SVST, "EBI")
    field (ZRSV, "MAJOR")
    field (THSV, "MAJOR")
    field (FVSV, "MAJOR")
    field (SXSV, "MAJOR")
    field (EISV, "MAJOR")
}

# Leaky bucket error counter
record(calcout, "$(P)$(R)#opl_US_EC_$(BRD)")
{
    field(DESC, "SCU OPL Upstream leaky err calc")
    field(INPA, "$(P)$(R)#r_conf_0x0060_$(BRD).VAL CPP")
    field(CALC, "A%65536")
    field(OUT,  "$(P)$(R)opl_US_EC_$(BRD)")
}
record(longin, "$(P)$(R)opl_US_EC_$(BRD)")
{
    field(DESC, "SCU OPL Upstream leaky err")
    field(INP,  "$(P)$(R)#opl_US_EC_$(BRD) CPP")
}

record(calcout, "$(P)$(R)#opl_US_EC_max_$(BRD)")
{
    field(DESC, "SCU OPL Upstream leaky err max calc")
    field(INPA, "$(P)$(R)#r_conf_0x00c0_$(BRD).VAL CPP")
    field(CALC, "A%65536")
    field(OUT,  "$(P)$(R)opl_US_EC_max_$(BRD)")
}
record(longin, "$(P)$(R)opl_US_EC_max_$(BRD)")
{
    field(DESC, "SCU OPL Upstream leaky err max")
    field(INP,  "$(P)$(R)#opl_US_EC_max_$(BRD) CPP")
}

# Lane/channel up, hard/soft errors
record(calcout, "$(P)$(R)#opl_US_lane_up_$(BRD)")
{
    field(DESC, "SCU OPL Upstream lane up calc")
    field(INPA, "$(P)$(R)#r_conf_0x0064_$(BRD).VAL CPP")
    field(CALC, "A%2")
    field(OUT,  "$(P)$(R)opl_US_lane_up_$(BRD)")
}
record(bi, "$(P)$(R)opl_US_lane_up_$(BRD)")
{
    field(DESC, "SCU OPL Upstream lane up")
    field(INP,  "$(P)$(R)#opl_US_lane_up_$(BRD) CPP")
    field(ZNAM, "Not OK")
    field(ONAM, "OK")
    field(ZSV,  "NO_ALARM")
    field(OSV,  "NO_ALARM")
}

record(calcout, "$(P)$(R)#opl_US_chan_up_$(BRD)")
{
    field(DESC, "SCU OPL Upstream chan up calc")
    field(INPA, "$(P)$(R)#r_conf_0x0064_$(BRD).VAL CPP")
    field(CALC, "(A>>1)%2")
    field(OUT,  "$(P)$(R)opl_US_chan_up_$(BRD)")
}
record(bi, "$(P)$(R)opl_US_chan_up_$(BRD)")
{
    field(DESC, "SCU OPL Upstream chan up")
    field(INP,  "$(P)$(R)#opl_US_chan_up_$(BRD) CPP")
    field(ZNAM, "Not OK")
    field(ONAM, "OK")
    field(ZSV,  "NO_ALARM")
    field(OSV,  "NO_ALARM")
}

record(calcout, "$(P)$(R)#opl_US_hard_err_$(BRD)")
{
    field(DESC, "SCU OPL Upstream hard err calc")
    field(INPA, "$(P)$(R)#r_conf_0x0064_$(BRD).VAL CPP")
    field(CALC, "(A>>2)%2")
    field(OUT,  "$(P)$(R)opl_US_hard_err_$(BRD)")
}
record(bi, "$(P)$(R)opl_US_hard_err_$(BRD)")
{
    field(DESC, "SCU OPL Upstream hard err")
    field(INP,  "$(P)$(R)#opl_US_hard_err_$(BRD) CPP")
    field(ZNAM, "OK")
    field(ONAM, "Not OK")
    field(ZSV,  "NO_ALARM")
    field(OSV,  "NO_ALARM")
}

record(calcout, "$(P)$(R)#opl_US_soft_err_$(BRD)")
{
    field(DESC, "SCU OPL Upstream soft err calc")
    field(INPA, "$(P)$(R)#r_conf_0x0064_$(BRD).VAL CPP")
    field(CALC, "(A>>3)%2")
    field(OUT,  "$(P)$(R)opl_US_soft_err_$(BRD)")
}
record(bi, "$(P)$(R)opl_US_soft_err_$(BRD)")
{
    field(DESC, "SCU OPL Upstream soft err")
    field(INP,  "$(P)$(R)#opl_US_soft_err_$(BRD) CPP")
    field(ZNAM, "OK")
    field(ONAM, "Not OK")
    field(ZSV,  "NO_ALARM")
    field(OSV,  "NO_ALARM")
}

# Lane/channel up, hard/soft errors counters
record(longin, "$(P)$(R)opl_US_soft_EC_$(BRD)")
{
    field(DESC, "SCU OPL Upstream chan down err cnt")
    field(INP,  "$(P)$(R)#r_conf_0x0068_$(BRD).VAL CPP")
}
record(longin, "$(P)$(R)opl_US_hard_EC_$(BRD)")
{
    field(DESC, "SCU OPL Upstream chan down err cnt")
    field(INP,  "$(P)$(R)#r_conf_0x006c_$(BRD).VAL CPP")
}
record(longin, "$(P)$(R)opl_US_ch_down_EC_$(BRD)")
{
    field(DESC, "SCU OPL Upstream chan down err cnt")
    field(INP,  "$(P)$(R)#r_conf_0x0070_$(BRD).VAL CPP")
}
record(longin, "$(P)$(R)opl_US_ln_down_EC_$(BRD)")
{
    field(DESC, "SCU OPL Upstream chan down err cnt")
    field(INP,  "$(P)$(R)#r_conf_0x0074_$(BRD).VAL CPP")
}

# Other errors counters
record(longin, "$(P)$(R)opl_US_enc_supervis_EC_$(BRD)")
{
    field(DESC, "SCU OPL Upstream chan down err cnt")
    field(INP,  "$(P)$(R)#r_conf_0x009c_$(BRD).VAL CPP")
}
record(longin, "$(P)$(R)opl_US_LS_EC_$(BRD)")
{
    field(DESC, "SCU OPL Upstream chan down err cnt")
    field(INP,  "$(P)$(R)#r_conf_0x00a0_$(BRD).VAL CPP")
}
record(longin, "$(P)$(R)opl_US_Rx_flag_EC_$(BRD)")
{
    field(DESC, "SCU OPL Upstream chan down err cnt")
    field(INP,  "$(P)$(R)#r_conf_0x00a4_$(BRD).VAL CPP")
}
record(longin, "$(P)$(R)opl_US_Tx_frm_drop_EC_$(BRD)")
{
    field(DESC, "SCU OPL Upstream chan down err cnt")
    field(INP,  "$(P)$(R)#r_conf_0x00a8_$(BRD).VAL CPP")
}
record(longin, "$(P)$(R)opl_US_Rx_frm_drop_EC_$(BRD)")
{
    field(DESC, "SCU OPL Upstream chan down err cnt")
    field(INP,  "$(P)$(R)#r_conf_0x00ac_$(BRD).VAL CPP")
}
record(longin, "$(P)$(R)opl_US_Rx_frm_EC_$(BRD)")
{
    field(DESC, "SCU OPL Upstream chan down err cnt")
    field(INP,  "$(P)$(R)#r_conf_0x00b0_$(BRD).VAL CPP")
}
record(longin, "$(P)$(R)opl_US_NB_ste_EC_$(BRD)")
{
    field(DESC, "SCU OPL Upstream neigh state err cnt")
    field(INP,  "$(P)$(R)#r_conf_0x00b4_$(BRD).VAL CPP")
}
record(longin, "$(P)$(R)opl_US_NBAN_ste_EC_$(BRD)")
{
    field(DESC, "SCU OPL Upstream neigh aft state err cnt")
    field(INP,  "$(P)$(R)#r_conf_0x00b8_$(BRD).VAL CPP")
}

# OPL Downstream ##########################################################

# OPL states
record(mbbi, "$(P)$(R)opl_DS_Tx_local_ste_$(BRD)")
{
    field (DESC, "OPL Downstream tx local state")
    field (DTYP, "Raw Soft Channel")
    field (INP,  "$(P)$(R)#r_conf_0x0044_$(BRD) CPP")
    field (NOBT, "21")
    field (MASK, "0x00000007")
    field (ZRST, "Error")
    field (ONST, "OK")
    field (TWST, "BI ")
    field (FRST, "RBI")
    field (SVST, "EBI")
    field (ZRSV, "MAJOR")
    field (THSV, "MAJOR")
    field (FVSV, "MAJOR")
    field (SXSV, "MAJOR")
    field (EISV, "MAJOR")
}
record(mbbi, "$(P)$(R)opl_DS_Tx_NB_ste_$(BRD)")
{
    field (DESC, "OPL Downstream tx neighbour state")
    field (DTYP, "Raw Soft Channel")
    field (INP,  "$(P)$(R)#r_conf_0x0044_$(BRD) CPP")
    field (NOBT, "24")
    field (MASK, "0x00000007")
    field (ZRST, "Error")
    field (ONST, "OK")
    field (TWST, "BI ")
    field (FRST, "RBI")
    field (SVST, "EBI")
    field (ZRSV, "MAJOR")
    field (THSV, "MAJOR")
    field (FVSV, "MAJOR")
    field (SXSV, "MAJOR")
    field (EISV, "MAJOR")
}
record(mbbi, "$(P)$(R)opl_DS_Rx_NB_ste_$(BRD)")
{
    field (DESC, "OPL Downstream rx neighbour state")
    field (DTYP, "Raw Soft Channel")
    field (INP,  "$(P)$(R)#r_conf_0x0044_$(BRD) CPP")
    field (NOBT, "3")
    field (MASK, "0x00000007")
    field (ZRST, "Error")
    field (ONST, "OK")
    field (TWST, "BI ")
    field (FRST, "RBI")
    field (SVST, "EBI")
    field (ZRSV, "MAJOR")
    field (THSV, "MAJOR")
    field (FVSV, "MAJOR")
    field (SXSV, "MAJOR")
    field (EISV, "MAJOR")
}
record(mbbi, "$(P)$(R)opl_DS_Rx_NBAN_ste_$(BRD)")
{
    field (DESC, "OPL Downstream rx neighbour next state")
    field (DTYP, "Raw Soft Channel")
    field (INP,  "$(P)$(R)#r_conf_0x0044_$(BRD) CPP")
    field (NOBT, "6")
    field (MASK, "0x00000007")
    field (ZRST, "Error")
    field (ONST, "OK")
    field (TWST, "BI ")
    field (FRST, "RBI")
    field (SVST, "EBI")
    field (ZRSV, "MAJOR")
    field (THSV, "MAJOR")
    field (FVSV, "MAJOR")
    field (SXSV, "MAJOR")
    field (EISV, "MAJOR")
}

# Leaky bucket error counter
record(calcout, "$(P)$(R)#opl_DS_EC_$(BRD)")
{
    field(DESC, "SCU OPL Downstream leaky err calc")
    field(INPA, "$(P)$(R)#r_conf_0x0048_$(BRD).VAL CPP")
    field(CALC, "A%65536")
    field(OUT,  "$(P)$(R)opl_DS_EC_$(BRD)")
}
record(longin, "$(P)$(R)opl_DS_EC_$(BRD)")
{
    field(DESC, "SCU OPL Downstream leaky err")
    field(INP,  "$(P)$(R)#opl_DS_EC_$(BRD) CPP")
}

record(calcout, "$(P)$(R)#opl_DS_EC_max_$(BRD)")
{
    field(DESC, "SCU OPL Downstream leaky err max calc")
    field(INPA, "$(P)$(R)#r_conf_0x00bc_$(BRD).VAL CPP")
    field(CALC, "A%65536")
    field(OUT,  "$(P)$(R)opl_DS_EC_max_$(BRD)")
}
record(longin, "$(P)$(R)opl_DS_EC_max_$(BRD)")
{
    field(DESC, "SCU OPL Downstream leaky err max")
    field(INP,  "$(P)$(R)#opl_DS_EC_max_$(BRD) CPP")
}

# Lane/channel up, hard/soft errors
record(calcout, "$(P)$(R)#opl_DS_lane_up_$(BRD)")
{
    field(DESC, "SCU OPL Downstream lane up calc")
    field(INPA, "$(P)$(R)#r_conf_0x004c_$(BRD).VAL CPP")
    field(CALC, "A%2")
    field(OUT,  "$(P)$(R)opl_DS_lane_up_$(BRD)")
}
record(bi, "$(P)$(R)opl_DS_lane_up_$(BRD)")
{
    field(DESC, "SCU OPL Downstream lane up")
    field(INP,  "$(P)$(R)#opl_DS_lane_up_$(BRD) CPP")
    field(ZNAM, "Not OK")
    field(ONAM, "OK")
    field(ZSV,  "NO_ALARM")
    field(OSV,  "NO_ALARM")
}

record(calcout, "$(P)$(R)#opl_DS_chan_up_$(BRD)")
{
    field(DESC, "SCU OPL Downstream chan up calc")
    field(INPA, "$(P)$(R)#r_conf_0x004c_$(BRD).VAL CPP")
    field(CALC, "(A>>1)%2")
    field(OUT,  "$(P)$(R)opl_DS_chan_up_$(BRD)")
}
record(bi, "$(P)$(R)opl_DS_chan_up_$(BRD)")
{
    field(DESC, "SCU OPL Downstream chan up")
    field(INP,  "$(P)$(R)#opl_DS_chan_up_$(BRD) CPP")
    field(ZNAM, "Not OK")
    field(ONAM, "OK")
    field(ZSV,  "NO_ALARM")
    field(OSV,  "NO_ALARM")
}

record(calcout, "$(P)$(R)#opl_DS_hard_err_$(BRD)")
{
    field(DESC, "SCU OPL Downstream hard err calc")
    field(INPA, "$(P)$(R)#r_conf_0x004c_$(BRD).VAL CPP")
    field(CALC, "(A>>2)%2")
    field(OUT,  "$(P)$(R)opl_DS_hard_err_$(BRD)")
}
record(bi, "$(P)$(R)opl_DS_hard_err_$(BRD)")
{
    field(DESC, "SCU OPL Downstream hard err")
    field(INP,  "$(P)$(R)#opl_DS_hard_err_$(BRD) CPP")
    field(ZNAM, "Not OK")
    field(ONAM, "OK")
    field(ZSV,  "NO_ALARM")
    field(OSV,  "NO_ALARM")
}

record(calcout, "$(P)$(R)#opl_DS_soft_err_$(BRD)")
{
    field(DESC, "SCU OPL Downstream soft err calc")
    field(INPA, "$(P)$(R)#r_conf_0x004c_$(BRD).VAL CPP")
    field(CALC, "(A>>3)%2")
    field(OUT,  "$(P)$(R)opl_DS_soft_err_$(BRD)")
}
record(bi, "$(P)$(R)opl_DS_soft_err_$(BRD)")
{
    field(DESC, "SCU OPL Downstream soft err")
    field(INP,  "$(P)$(R)#opl_DS_soft_err_$(BRD) CPP")
    field(ZNAM, "Not OK")
    field(ONAM, "OK")
    field(ZSV,  "NO_ALARM")
    field(OSV,  "NO_ALARM")
}

# Lane/channel up, hard/soft errors counters
record(longin, "$(P)$(R)opl_DS_soft_EC_$(BRD)")
{
    field(DESC, "SCU OPL Downstream chan down err cnt")
    field(INP,  "$(P)$(R)#r_conf_0x0050_$(BRD).VAL CPP")
}
record(longin, "$(P)$(R)opl_DS_hard_EC_$(BRD)")
{
    field(DESC, "SCU OPL Downstream chan down err cnt")
    field(INP,  "$(P)$(R)#r_conf_0x0054_$(BRD).VAL CPP")
}
record(longin, "$(P)$(R)opl_DS_ch_down_EC_$(BRD)")
{
    field(DESC, "SCU OPL Downstream chan down err cnt")
    field(INP,  "$(P)$(R)#r_conf_0x0058_$(BRD).VAL CPP")
}
record(longin, "$(P)$(R)opl_DS_ln_down_EC_$(BRD)")
{
    field(DESC, "SCU OPL Downstream chan down err cnt")
    field(INP,  "$(P)$(R)#r_conf_0x005c_$(BRD).VAL CPP")
}

# Other errors counters
record(longin, "$(P)$(R)opl_DS_enc_supervis_EC_$(BRD)")
{
    field(DESC, "SCU OPL Downstream chan down err cnt")
    field(INP,  "$(P)$(R)#r_conf_0x007c_$(BRD).VAL CPP")
}
record(longin, "$(P)$(R)opl_DS_LS_EC_$(BRD)")
{
    field(DESC, "SCU OPL Downstream chan down err cnt")
    field(INP,  "$(P)$(R)#r_conf_0x0080_$(BRD).VAL CPP")
}
record(longin, "$(P)$(R)opl_DS_Rx_flag_EC_$(BRD)")
{
    field(DESC, "SCU OPL Downstream chan down err cnt")
    field(INP,  "$(P)$(R)#r_conf_0x0084_$(BRD).VAL CPP")
}
record(longin, "$(P)$(R)opl_DS_Tx_frm_drop_EC_$(BRD)")
{
    field(DESC, "SCU OPL Downstream chan down err cnt")
    field(INP,  "$(P)$(R)#r_conf_0x0088_$(BRD).VAL CPP")
}
record(longin, "$(P)$(R)opl_DS_Rx_frm_drop_EC_$(BRD)")
{
    field(DESC, "SCU OPL Downstream chan down err cnt")
    field(INP,  "$(P)$(R)#r_conf_0x008c_$(BRD).VAL CPP")
}
record(longin, "$(P)$(R)opl_DS_Rx_frm_EC_$(BRD)")
{
    field(DESC, "SCU OPL Downstream chan down err cnt")
    field(INP,  "$(P)$(R)#r_conf_0x0090_$(BRD).VAL CPP")
}
record(longin, "$(P)$(R)opl_DS_NB_ste_EC_$(BRD)")
{
    field(DESC, "SCU OPL Downstream neigh state err cnt")
    field(INP,  "$(P)$(R)#r_conf_0x0094_$(BRD).VAL CPP")
}
record(longin, "$(P)$(R)opl_DS_NBAN_ste_EC_$(BRD)")
{
    field(DESC, "SCU OPL Downstr neigh aft state err cnt")
    field(INP,  "$(P)$(R)#r_conf_0x0098_$(BRD).VAL CPP")
}
