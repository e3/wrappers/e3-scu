## I2C EEPROM Cooling Unit data reading
# read_i2c parameters:
# - Slot is always 13
# - Data size = 118
# - PVs of each data to retreive from the lsi


record(lsi, "$(P)$(R)i2c_eeprom_cu")
{
	field(DESC, "I2C reading cooling unit eeprom")
	field(DTYP, "stream")
	field(INP,  "@scu.proto read_eeprom_cu($(P)$(R)i2c_eeprom_manuf, $(P)$(R)i2c_eeprom_name, $(P)$(R)i2c_eeprom_rev, $(P)$(R)i2c_eeprom_sn, $(P)$(R)#i2c_eeprom_op_hrs, $(P)$(R)i2c_eeprom_uid, $(P)$(R)#i2c_eeprom_op_hrsA, $(P)$(R)#i2c_eeprom_op_hrsB) $(PORT)")
	field(SIZV, "118")
	field(PINI, "1")
	field(SCAN, "2 second")
}

# Manufacturer
record(stringin, "$(P)$(R)i2c_eeprom_manuf")
{
	field(DESC, "I2C reading eeprom manuf")
}

# type Name
record(stringin, "$(P)$(R)i2c_eeprom_name")
{
	field(DESC, "I2C reading eeprom name")
}

# revision
record(stringin, "$(P)$(R)i2c_eeprom_rev")
{
	field(DESC, "I2C reading eeprom rev")
}

# Serial number
record(stringin, "$(P)$(R)i2c_eeprom_sn")
{
	field(DESC, "I2C reading eeprom S/N")
}

# Operating hours
record(longin, "$(P)$(R)#i2c_eeprom_op_hrs")
{
	field(DESC, "I2C eeprom op hrs")
}
record(calcout, "$(P)$(R)#i2c_eeprom_op_hrs_calc")
{
 	field(DESC, "I2C eeprom op hrs calc")
 	field(INPA, "$(P)$(R)#i2c_eeprom_op_hrs CPP")
	field(CALC, "(A/4)")
	field(OUT,  "$(P)$(R)i2c_eeprom_op_hrs PP")
}
record(ai, "$(P)$(R)i2c_eeprom_op_hrs")
{
	field(DESC, "I2C reading eeprom op hrs")
	field(EGU, "h")
}

# Unique ID
record(longin, "$(P)$(R)i2c_eeprom_uid")
{
	field(DESC, "I2C reading eeprom uid")
}

# Operating hours fan A
record(longin, "$(P)$(R)#i2c_eeprom_op_hrsA")
{
	field(DESC, "I2C eeprom op hrs fanA")
}
record(calcout, "$(P)$(R)#i2c_eeprom_op_hrsA_calc")
{
 	field(DESC, "I2C eeprom op hrs fanA calc")
 	field(INPA, "$(P)$(R)#i2c_eeprom_op_hrsA CPP")
	field(CALC, "(A/4)")
	field(OUT,  "$(P)$(R)i2c_eeprom_op_hrs_A PP")
}
record(ai, "$(P)$(R)i2c_eeprom_op_hrs_A")
{
	field(DESC, "I2C reading eeprom op hrs fanA")
	field(EGU, "h")
}

# Operating hours fan B
record(longin, "$(P)$(R)#i2c_eeprom_op_hrsB")
{
	field(DESC, "I2C eeprom op hrs fanB")
}
record(calcout, "$(P)$(R)#i2c_eeprom_op_hrsB_calc")
{
 	field(DESC, "I2C eeprom op hrs fanB calc")
 	field(INPA, "$(P)$(R)#i2c_eeprom_op_hrsB CPP")
	field(CALC, "(A/4)")
	field(OUT,  "$(P)$(R)i2c_eeprom_op_hrs_B PP")
}
record(ai, "$(P)$(R)i2c_eeprom_op_hrs_B")
{
	field(DESC, "I2C reading eeprom op hrs fanB")
	field(EGU, "h")
}

## I2C EEPROM data writing
# write_i2c parameters:
# - Slot # of the Device
# - Field to write = 1(Manufacturer), 2(Name), 3(Revision), 4(Serial number), 5(Operating hours), 6 (Fan A operating hours), 7 (Fan B operating hours)
# - Value to write

record(longout, "$(P)$(R)i2c_eeprom_f_select")
{
	field(DESC, "I2C writing eeprom")
	field(FLNK, "$(P)$(R)#eeprom_mux PP")
}

record(aSub,"$(P)$(R)#eeprom_mux")
{
	field(DESC, "Data Multiplexer to write the eeprom")
	field(SNAM, "eeprom_mux")

	field(INPA, "$(P)$(R)i2c_eeprom_f_select")
	field(FTA,  "LONG")
	field(NOA,  "1")

	field(INPB, "$(P)$(R)i2c_eeprom_manuf")
	field(FTB,  "STRING")
	field(NOB,  "1")

	field(INPC, "$(P)$(R)i2c_eeprom_name")
	field(FTC,  "STRING")
	field(NOC,  "1")

	field(INPD, "$(P)$(R)i2c_eeprom_rev")
	field(FTD,  "STRING")
	field(NOD,  "1")

	field(INPE, "$(P)$(R)i2c_eeprom_sn")
	field(FTE,  "STRING")
	field(NOE,  "1")

	field(INPF, "$(P)$(R)i2c_eeprom_op_hrs")
	field(FTF,  "LONG")
	field(NOF,  "1")

	field(INPG, "$(P)$(R)i2c_eeprom_op_hrsA")
	field(FTG,  "LONG")
	field(NOG,  "1")

	field(INPH, "$(P)$(R)i2c_eeprom_op_hrsB")
	field(FTH,  "LONG")
	field(NOH,  "1")

# field used for the MAC address in the Serializer, set here to 0 as not used
	field(INPI, "0")
	field(FTI,  "LONG")
	field(NOI,  "1")

	field(FTVA, "STRING")
	field(NOVA, "1")
	field(NEVA, "1")
	field(OUTA,  "$(P)$(R)i2c_eeprom_data_w PP")
}

record(stringout, "$(P)$(R)i2c_eeprom_data_w")
{
	field(DESC, "I2C writing eeprom")
	field(OUT, "$(P)$(R)i2c_eeprom_wr PP")
}

record(lso, "$(P)$(R)i2c_eeprom_wr")
{
	field(DESC, "I2C writing eeprom")
	field(DTYP, "stream")
	field(OUT,  "@scu.proto write_i2c_eeprom($(SLOT), $(P)$(R)i2c_eeprom_f_select, $(P)$(R)i2c_eeprom_data_w, $(P)$(R)#i2c_wr_eeprom_r) $(PORT)")
	field(SIZV, "38")
}

record(longin, "$(P)$(R)#i2c_wr_eeprom_r")
{
	field(DESC, "I2C writing eeprom return")
}
