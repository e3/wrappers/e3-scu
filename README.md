# e3-scu

EPICS IOC for an SCU, streamdevice based.

## Description

The **e3-scu** module is designed for monitoring and controlling registers on an FPGA, as well as managing and monitoring the crate. This module provides a unified interface, enabling both channels to operate together through a single command file.

### Key Components

There are two primary IOC shell (iocsh) files included:

- **FPGA Interface File**: Loads all process variables (PVs) required for interfacing with the FPGA.
- **Crate Management File**: Loads all PVs related to crate management tasks, such as I2C communication.

### Requests and Communication

Different types of requests are supported and implemented via a `.proto` file, as the module is based on StreamDevice. Depending on the request type, a server running on the CPUs of the MPSoS Zynq (located in the serializer) responds to the client accordingly. The IOC continuously polls this server to maintain updated data.

### Database Files (Db Folder)

In the **Db** folder, you’ll find various substitution and template files:

- **Substitutions for Each Type of MC**:
   - A substitution file that links to template files for processing raw data from FPGA registers.
   - A `max_mctype` substitution file for crate management tasks, such as reading temperature, voltage, and current values.

Additionally, the following templates and substitutions are available:

- **S-Link Information**: Reads data from the S-Link.
- **OPL Information**: Retrieves OPL-related data.
- **History Buffer Information**: Reads data from the history buffer.


## Usage (locally)

```sh
make cellinstall
$ iocsh cmds/*.cmd -l cellMods/
```
